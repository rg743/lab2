package gradingsystem;

import javafx.application.Application; 
import static javafx.application.Application.launch;
import javafx.geometry.Insets; 
import javafx.geometry.Pos; 
import javafx.scene.Scene; 
import javafx.scene.control.Button; 
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane; 
import javafx.scene.text.Text; 
import javafx.scene.control.TextField; 
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;  

public class GradingSystem extends Application {

    TextField inClassResult;
    int inClassGrade;
    TextField assignment1Result;
    int assignment1Grade;
    TextField assignment2Result;
    int assignment2Grade;
    TextField assignment3Result;
    int assignment3Grade;
    double finalResult;
    int finalResultGrade;
    String msgResult;
        
    @Override
    public void start(Stage primaryStage) {
    try {        
            primaryStage.setTitle("Student Grading System");
        
            GridPane grid = new GridPane();
            grid.setAlignment(Pos.CENTER);
            grid.setHgap(10);
            grid.setVgap(10);
            grid.setPadding(new Insets(25, 25, 25, 25));

            Text scenetitle = new Text("Welcome to the Student Grading System");
            scenetitle.setFont (Font.font("Arial", FontWeight.NORMAL, 20));
            grid.add(scenetitle, 0, 0, 2, 1);

            Label inClassTest = new Label("In-class tests result: ");
            grid.add(inClassTest, 0, 1);

            inClassResult = new TextField();
            grid.add(inClassResult, 1, 1);           

            Label assignment1 = new Label("Assignment One result: ");
            grid.add(assignment1, 0, 2);

            assignment1Result = new TextField();
            grid.add(assignment1Result, 1, 2);           

            Label assignment2 = new Label("Assignment Two result: ");
            grid.add(assignment2, 0, 3);

            assignment2Result = new TextField();
            grid.add(assignment2Result, 1, 3);            

            Label assignment3 = new Label("Assignment Three result: ");
            grid.add(assignment3, 0, 4);

            assignment3Result = new TextField();
            grid.add(assignment3Result, 1, 4);            

            Button btnGetResult = new Button("Get Final Result");
            HBox hbBtn = new HBox(10);
            hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
            hbBtn.getChildren().add(btnGetResult);
            grid.add(hbBtn, 1, 5);
            
            btnGetResult.setOnAction(e ->{
                calculate();
                AlertBox.display("Final Results", msgResult);                
            });

            Scene scene = new Scene(grid, 420, 250);

            primaryStage.setScene(scene);
            primaryStage.show();
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }

    public String calculate(){
        int inClass = Integer.parseInt(inClassResult.getText());
        int assignment1 = Integer.parseInt(assignment1Result.getText());
        int assignment2 = Integer.parseInt(assignment2Result.getText());
        int assignment3 = Integer.parseInt(assignment3Result.getText());

        finalResult = (inClass * 0.2) + (assignment1 * 0.25) + (assignment2 * 0.25) + (assignment3 * 0.3);
        int result = (int) Math.round(finalResult);

        int [] arrayResult = {inClass, assignment1, assignment2, assignment3, result};
        int[] arrayGrade = new int[5];
        int j = 0;
        
        for (int i : arrayResult){            
            if (i <= 100 && i >= 80){
                arrayGrade[j] = 1;
                ++j;                    
            } else if (i < 80 && i >= 77){
                arrayGrade[j] = 2;
                ++j;
            } else if (i < 77 && i >= 73){
                arrayGrade[j] = 3;
                ++j;
            } else if (i < 73 && i >= 70){
                arrayGrade[j] = 4;
                ++j;
            } else if (i < 70 && i >= 68){
                arrayGrade[j] = 5;
                ++j;
            } else if (i < 68 && i >= 65){
                arrayGrade[j] = 6;
                ++j;
            } else if (i < 65 && i >= 63){
                arrayGrade[j] = 7;
                ++j;
            } else if (i < 63 && i >= 60){
                arrayGrade[j] = 8;
                ++j;
            } else if (i < 60 && i >= 58){
                arrayGrade[j] = 9;
                ++j;
            } else if (i < 58 && i >= 55){
                arrayGrade[j] = 10;
                ++j;
            } else if (i < 55 && i >= 53){
                arrayGrade[j] = 11;
                ++j;
            } else if (i < 53 && i >= 50){
                arrayGrade[j] = 12;
                ++j;
            } else if (i < 50 && i >= 48){
                arrayGrade[j] = 13;
                ++j;
            } else if (i < 48 && i >= 45){
                arrayGrade[j] = 14;
                ++j;
            } else if (i < 45 && i >= 43){
                arrayGrade[j] = 15;
                ++j;
            } else if (i < 43 && i >= 40){
                arrayGrade[j] = 16;
                ++j;
            } else if (i < 40 && i >= 38){
                arrayGrade[j] = 17;
                ++j;
            } else if (i < 38 && i >= 35){
                arrayGrade[j] = 18;
                ++j;
            } else if (i < 35 && i >= 0){
                arrayGrade[j] = 19;
                ++j;
            } else{
                System.out.println("Enter valid grades");
            }
        }

        inClassGrade = arrayGrade[0];
        assignment1Grade = arrayGrade[1];
        assignment2Grade = arrayGrade[2];
        assignment3Grade = arrayGrade[3];
        finalResultGrade = arrayGrade[4];

        msgResult = "In-Class.\n\tResult: " + inClass + "%\tGrade: " + inClassGrade
                + "\n\nAssignment One.\n\tResult: " + assignment1 + "%\tGrade: " + assignment1Grade
                + "\n\nAssignment Two.\n\tResult: " + assignment2 + "%\tGrade: " + assignment2Grade
                + "\n\nAssignment Three.\n\tResult: " + assignment3 + "%\tGrade: " + assignment3Grade
                + "\n\nFinal Result.\n\tResult: " + result + "%\tGrade: " + finalResultGrade;
        
        return msgResult;
    }
}